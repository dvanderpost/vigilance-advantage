/*
Copyright 2013 Daniel J. van der Post

This file is part of Vigilance_advantage.

Vigilance_advantage is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Vigilance_advantage is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "graphics.h"
#include "par.h"
#include "model.h"

//external variables from model.cpp
extern int background;
extern int c_quittime;
extern int simtime;

int main(int argc, char *argv[])
{
  //Read command arguments and "background"
  Start(argc,argv);  //in model.cpp

  if (!background) initGLUT(argc, argv); //in graphics.cpp
  Initialize();      //in model.cpp

  while(simtime < c_quittime)
    {
      Run();         //in model.cpp
      if (!background) GlutStep();  //in graphics.cpp

      //Keep track of time = every run step
      simtime++;
    }
 return(0);
}

