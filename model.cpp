/*
Copyright 2013 Daniel J. van der Post

This file is part of Vigilance_advantage.

Vigilance_advantage is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Vigilance_advantage is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "model.h"
#include <boost/random.hpp>

//RUNNING MODEL
void StartError(int num);
bool ChooseNewAction();
void StartNewAttack();
void ForagerPositions(int i);

//BEHAVIOUR
bool ForagerAction();
bool PredatorAction();
void PredatorMove(int i, int closest);
int PredatorClosestTarget(int i);
int PredatorFurthestTarget(int i);
int PredatorRandomTarget(int i);
float PredatorTargetDistance(int i, int target);
bool PredatorAttack(int i, int closest);
float PredatorDetectionProb(float distance);

//DATA
void StoreTargetDistances(int closest);
void StoreTargetandAttackDistances(int closest);
void GroupSpread();
void EndandPrintData();

float PROB_VIGILANCE = 0.1;
int RANDOMSEED = 45693;
int NUMBER_FORAGERS = 20;
int NUMBER_VIGILANT =  5;
float GROUP_RADIUS =  10;
float CHANGE_POSITION_RATE = 0.0;
int NUMBER_OF_ATTACKS = 1000000;
bool PAIRWISE=false;
bool SIGMOID = true;      
float ALPHA = 1.0;
float NN = 2.0;
float H = 20.0;
float H_N = pow(H,NN);
bool background = false;

//RANDOM NUMBER GENERATOR
boost::mt19937 rng(RANDOMSEED);
boost::uniform_real<> ud(0.0, 1.0);
boost::variate_generator<boost::mt19937&,boost::uniform_real<> > var_uni(rng,ud);

int simtime = 0;
int resetevents = 0;
int predatormoves = 0;
float gl_avgdist = 0;
float maxdist = 0;
int attacks = 0;
int global_target = -1;

Animal_type foragers[MAX_FORAGERS];
Animal_type predators[NUMBER_PREDATORS];

//FUNCTION DEFINITIONS

void Start(int argc, char *argv[])
{
  int i;
  
  //parse command line options if any included
  if (argc < 25 || argc > 26)
    StartError(argc);
  else
    {
      //go through any arguments included
      for(i = 1; i < argc; ++i)
        {
	  if (argv[i][0] == '-')
            {
	      switch(argv[i][1])
		{
		case 'b': //1
		  background = true; //run in background
		  break;
		case 'V': //2
		  PROB_VIGILANCE = atof(argv[i+1]);
		  break;
		case 'G': //3
		  NUMBER_FORAGERS = atoi(argv[i+1]);
		  break;
		case 'N': //4
		  NUMBER_VIGILANT = atoi(argv[i+1]);
		  break;
		case 'S': //5
		  RANDOMSEED = atoi(argv[i+1]);
		  var_uni.engine().seed(RANDOMSEED);
		  var_uni.distribution().reset();
		  break;
		case 'R': //6
		  GROUP_RADIUS = atof(argv[i+1]);
		  break;
		case 'C': //7
		  CHANGE_POSITION_RATE = atof(argv[i+1]);
		  break;
		case 'A': //8
		  NUMBER_OF_ATTACKS = atoi(argv[i+1]);
		  break;
		case 'P': //9
		  if(atoi(argv[i+1])==1)
		    PAIRWISE = true;
		  else
		    PAIRWISE = false;
		  break;
		case 'H': //10
		  H = atof(argv[i+1]);
		  break;
		case 'Z': //11
		  NN = atof(argv[i+1]);
		  break;
		case 'Y': //12
		  ALPHA = atof(argv[i+1]);
		  break;
		case 'F': //13
		  if(atoi(argv[i+1])==1)
		    SIGMOID = true;
		  else
		    SIGMOID = false;
		  break;
		default:
		  {
		    StartError(argc);
		    break;
		  }
                }
            }
        }
    }
  H_N = pow(H,NN);
}

void StartError(int num)
{
  cout << "ERROR - wrong number of command line options or wrong option used:" <<num<<endl;
    cout << "USAGE:" << endl;
    cout << "./model\n";
    cout << "plus all the following options with value:\n";
    cout << "-S [integer] = random seed\n";
    cout << "-V [0-1] = vigilance level of vigilant individuals\n";
    cout << "-G [integer] = group size\n";
    cout << "-N [integer] = number of vigilant individuals <= G\n";
    cout << "-R [positive value] = radius of group\n";
    cout << "-A [integer] = number of attacks\n";
    cout << "-C [0-1] = rate of changing positions\n";
    cout << "-P [1 or !1] = if 1 then does pairwise simulation, with condition G=2, N=1\n";
    cout << "-Y [0-1] = alpha; saturation point of detection function\n";
    cout << "-H [positive value] = h; halfmax value of detection function\n";
    cout << "-Z [positive value] = N; power that scales steepness of detection function\n";
    cout << "-F [1 or !1] = if 1 then does sigmoid function, else linear\n";
    cout << "Optional:\n";
    cout << "-b = for running without graphics (\"background\")\n";
    exit(0);
}


void Run()
{
    bool reset = false;
    reset = ChooseNewAction();

    //reset to initial positions when one flees or one caught
    if (reset)
    {
        if (resetevents == NUMBER_OF_ATTACKS)
            EndandPrintData();
        else
            StartNewAttack();
    }
}

bool ChooseNewAction()
{
    bool reset = false;

    reset = ForagerAction();
    for(int i = 0; i < NUMBER_FORAGERS; ++i)
        if (var_uni() < CHANGE_POSITION_RATE)
            ForagerPositions(i);

    if (!reset)
        reset = PredatorAction();

    return reset;
}

void StartNewAttack()
{
    //reposition predator
    predators[0].px = 0;
    predators[0].py = GRIDY/2;
    predatormoves = 0;
    global_target = -1;

    //new positions foragers + all doing nothing
    for(int i = 0; i < NUMBER_FORAGERS; ++i)
    {
        ForagerPositions(i);
        foragers[i].action = NOTHING;
        foragers[i].scan_events = 0;
    }
    GroupSpread(); //get data on group spread

    //count number of resets
    ++resetevents;
    simtime = 0;
}

void EndandPrintData()
{
    for(int f = 0; f<NUMBER_FORAGERS; ++f)
    {
      if(foragers[f].predation_events>0)
	{
	  cout << NUMBER_FORAGERS << " " << NUMBER_VIGILANT << " " << GROUP_RADIUS << " " << PROB_VIGILANCE << " " << f << " " << foragers[f].pV << " " << foragers[f].predation_events << " " << (float)foragers[f].predation_events/resetevents  << " " << foragers[f].avg_dist_vig_caught/foragers[f].predation_events << " " << foragers[f].avg_dist_vig_target/foragers[f].target_events << " " << gl_avgdist/attacks << " " << maxdist/attacks << " " << CHANGE_POSITION_RATE << " " << foragers[f].avg_nearest_dist_vig/foragers[f].target_events << endl;
	}
      else
	{
	  cout << NUMBER_FORAGERS << " " << NUMBER_VIGILANT << " " << GROUP_RADIUS << " " << PROB_VIGILANCE << " " << f << " " << foragers[f].pV << " " << foragers[f].predation_events << " " << (float)foragers[f].predation_events/resetevents  << " " << 0.0 << " " << foragers[f].avg_dist_vig_target/foragers[f].target_events << " " << gl_avgdist/attacks << " " << maxdist/attacks << " " << CHANGE_POSITION_RATE << " " << foragers[f].avg_nearest_dist_vig/foragers[f].target_events << endl;
	}
    }
    //resetevents=0;
    simtime = QUITTIME;
}

int PredatorFurthestTarget(int i)
{
  float max_dist = 0;
  int furthest = 0;
  for(int j = 0; j < NUMBER_FORAGERS; ++j)
    {
      float dist = foragers[j].Distance(predators[i].px, predators[i].py);
      if (j == 0)
	{
	  max_dist = dist;
	  furthest = j;
	}
      else if (dist > max_dist)
	{
	  max_dist = dist;
	  furthest = j;
	}
    }
  return furthest;
}

int PredatorClosestTarget(int i)
{
  float min_dist = 0;
  int closest = 0;
  int closest_array[MAX_FORAGERS], counter=0;
  for(int j = 0; j < NUMBER_FORAGERS; ++j)
    {
      float dist = foragers[j].Distance(predators[i].px, predators[i].py);
      if (j == 0)
	{
	  min_dist = dist;
	  closest = j;
	  closest_array[counter]=closest; counter++;
	}
      else 
	{
	  if (dist < min_dist)
	    {
	      min_dist = dist;
	      closest = j;
	      closest_array[0]=closest; counter=1;
	    }
	  else if (dist == min_dist)
	    {
	      //cout<<"same dist";
	      if(var_uni()<0.5)
	      closest = j;
	      closest_array[counter]=j; counter++;
	    }
	}
    }
  if(counter>1) closest = closest_array[(int)(var_uni()*counter)];
  return closest;
}

int PredatorRandomTarget(int i)
{
  return (var_uni()*NUMBER_FORAGERS);
}

float PredatorTargetDistance(int i, int target)
{
  return foragers[target].Distance(predators[i].px, predators[i].py);
}

bool PredatorAction()
{
    bool reset = false;
    int target = -1;

    for(int i = 0; i < NUMBER_PREDATORS; ++i)
      {      
	switch (predatortargetting) //defined in par.h
	  {
	  case CLOSEST:
	    target = PredatorClosestTarget(i);
	    break;
	  case FURTHEST:  //once only for fixed groups
	    if(global_target == -1)
	      global_target = PredatorFurthestTarget(i);
	    target = global_target;
	    break;
	  case RANDOM:
	    if(global_target == -1)
	      global_target = PredatorRandomTarget(i);	   
	    target = global_target;
	    break;
	  default:
	    target = PredatorClosestTarget(i);
	    break;	
	  }

	float target_dist = PredatorTargetDistance(i, target);

        //if closer than pD then attack
        if (target_dist <= PRED_ATTACK_DISTANCE)
	  {
            reset = PredatorAttack(i, target);
            StoreTargetandAttackDistances(target);
	  }
        //else move forward
        else
	  {
            PredatorMove(i, target);
            StoreTargetDistances(target);
	  }
      }
    return reset;
}

void StoreTargetandAttackDistances(int target)
{
    //get average distance to vigilant individuals:
    float avgdist = 0;
    int count = 0;
    float cdist = GRIDX;
    int closest = -1;
    for(int j = 0; j<NUMBER_FORAGERS; ++j)
    {
        if (j != target && foragers[j].pV > 0)
        {
	  float dist = foragers[j].Distance(foragers[target].px, foragers[target].py);
	  if(dist<cdist)
	    {closest = j; cdist=dist;}
	  avgdist += dist;
	  ++count;
        }
    }
    foragers[target].avg_nearest_dist_vig += cdist;
    foragers[target].avg_dist_vig_caught += avgdist/count;
    foragers[target].avg_dist_vig_target += avgdist/count;
    ++foragers[target].target_events;
}

void StoreTargetDistances(int target)
{
    //get average distance to vigilant individuals:
    if (simtime == 1 || CHANGE_POSITION_RATE > 0.0)
    {
        float avgdist = 0;
        int count = 0;
	float cdist = GRIDX;
	int closest = -1;
        for(int j = 0; j < NUMBER_FORAGERS; ++j)
        {
            if (j != target && foragers[j].pV > 0)
            {
	      float dist = foragers[j].Distance(foragers[target].px, foragers[target].py);
	      if(dist<cdist)
		{closest = j; cdist=dist;}
	      avgdist += dist;
	      ++count;
            }
        }    
	foragers[target].avg_nearest_dist_vig += cdist;
        foragers[target].avg_dist_vig_target += avgdist/count;
        ++foragers[target].target_events;
    }
}

bool PredatorAttack(int i, int target)
{
    bool reset = false;

    predators[i].action = ATTACK;
    reset = true;
    ++foragers[target].predation_events;

    return reset;
}

void PredatorMove(int i, int target)
{
    //get heading towards closest foragers
    float d = predators[i].Distance(foragers[target].px, foragers[target].py);
    predators[i].hx = (foragers[target].px - predators[i].px) / d;
    predators[i].hy = (foragers[target].py - predators[i].py) / d;

    predators[i].action=MOVE;

    predators[i].px = predators[0].px + predators[i].hx;
    predators[i].py = predators[0].py + predators[i].hy;

    ++predatormoves;
}

bool ForagerAction()
{
    bool reset = false;

    for(int i=0; i < NUMBER_FORAGERS; ++i)
    {
        if (foragers[i].action == NOTHING)
        {
            if (var_uni() < foragers[i].pV)
                foragers[i].action = SCAN;
            //else remains NOTHING
        }
        else //if did scan
        {
            ++foragers[i].scan_events;
            float distance = foragers[i].Distance(predators[0].px, predators[0].py);
            if (distance <= MAX_DETECT_DISTANCE && var_uni() < PredatorDetectionProb(distance))
            {
                foragers[i].action = FLEE;
                reset = true; //activation of neighbors is implicit assumed
            }
            else if (var_uni() < foragers[i].pV)
                foragers[i].action = SCAN;
            else
                foragers[i].action = NOTHING;
        }
    }

    return reset;
}

void GroupSpread()
{
  float avgdist = 0;
  float t_maxdist = 0;
  float tempdist = 0;
  int counter = 0;
  
  for(int a = 0; a<NUMBER_FORAGERS; ++a)
    for(int b = a + 1; b < NUMBER_FORAGERS; ++b)
      {
	if (a != b)
	  {
	    tempdist = foragers[a].Distance(foragers[b].px, foragers[b].py);
	    if (tempdist > t_maxdist) t_maxdist = tempdist;
	    avgdist += tempdist;
	    ++counter;
	  }
      }
  gl_avgdist += avgdist/counter;
  maxdist += t_maxdist;
  ++attacks;
}

float PredatorDetectionProb(float distance)
{
    float prob = 0;
    if (SIGMOID)
        prob = ALPHA * ( H_N/ ( H_N + pow(distance,NN) ) );
    else //linear
        prob = ALPHA * (1 - (distance/H));
    return prob;
}


void Initialize()
{
    int id_counter = 0;

    //for number of aapjes
    // do Init
    for(int i = 0; i<NUMBER_FORAGERS; ++i)
    {
        float px=GRIDX/2;
        float py = GRIDY/2;
        float hx = 0;
        float hy = 0;
        float pV = 0;
        if (i<NUMBER_VIGILANT) pV = PROB_VIGILANCE;
        foragers[i].Init(px, py, hx, hy, pV, &id_counter, simtime);
        ForagerPositions(i);
    }
    GroupSpread();

    //for number of predators
    for(int i = 0; i<NUMBER_PREDATORS; ++i)
    {
        float px = 0;
        float py = GRIDY / 2;
        float hx = -1;
        float hy = 0;
        float pV = 0;
        predators[i].Init(px, py, hx, hy, pV, &id_counter, simtime);
    }
}

void ForagerPositions(int i)
{
  if(!PAIRWISE)
    {
      if (RANDOMVECTOR)
	{
	  //random unit vector
	  float ran = 2*PI*var_uni();
	  float vx = sin(ran);
	  float vy = cos(ran);
	  float dis = var_uni()*GROUP_RADIUS;
	  
	  foragers[i].px = (GRIDX/2) + vx*dis;
	  foragers[i].py = (GRIDY/2) + vy*dis;
	}
      else //in sqaure + test if in circle
	{
	  bool placed = false;
	  float startxy = ((GRIDX/2) - (GROUP_RADIUS)); //half group radius away in x and y direction
	  
	  while(!placed)
	    {
	      float ranx = (var_uni()*(GROUP_RADIUS*2)) + startxy;
	      float rany = (var_uni()*(GROUP_RADIUS*2)) + startxy;
	      
	      float dist = sqrt(((ranx - (GRIDX/2))*(ranx - (GRIDX/2)))+((rany - (GRIDX/2))*(rany - (GRIDX/2))));
	      
	      if (dist <= GROUP_RADIUS)
		{
		  foragers[i].px = ranx;
		  foragers[i].py = rany;
		  placed = true;
		}
	    }
	}
    }
  else
    {
      if(foragers[i].pV == 0.0)
	{
	  foragers[i].px = GRIDX/2;
	  foragers[i].py = GRIDY/2;
	}
      else
	{
	  foragers[i].px = GRIDX/2 + GROUP_RADIUS;
	  foragers[i].py = GRIDY/2;
	}
    }
}

void Animal_type::Init(float ppx, float ppy, float hhx, float hhy, float ppV, int* idd, int simtime)
{
    id = *idd;
    *idd = *idd + 1;
    comptime = simtime;
    px = ppx;
    py = ppy;
    hx = hhx;
    hy = hhy;
    action = NOTHING;
    pV = ppV;
    predation_events = 0;
    avg_dist_vig_caught = 0;
    avg_dist_vig_target = 0;
    avg_nearest_dist_vig = 0;
    scan_events = 0;
    target_events = 0;
}

float Animal_type::Distance(float hhx, float hhy)
{
    return sqrt(((px-hhx)*(px-hhx))+((py-hhy)*(py-hhy)));
}
