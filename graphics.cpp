/*
Copyright 2013 Daniel J. van der Post

This file is part of Vigilance_advantage.

Vigilance_advantage is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Vigilance_advantage is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "graphics.h"
#include "model.h"
#include "par.h"
#include <math.h>

#define INFORMATION "" //for function ShowIt

extern int simtime;
extern int NUMBER_FORAGERS;
extern int GROUP_RADIUS;
extern Animal_type foragers[MAX_FORAGERS];
extern Animal_type predators[NUMBER_PREDATORS];
extern bool background;
int c_quittime=QUITTIME;

int windowscaler_x = 3;
int windowscaler_y = 3;
int  gridx = GRIDX > 1600 ? 1600:GRIDX*windowscaler_x;
int  gridy = GRIDY > 1200 ? 1200:GRIDY*windowscaler_y;

static int one_step = false; //for Quit()
static GLfloat increase = 10.;
static GLfloat factor = 10.;

int rwidth = GRIDX;
int rheight = GRIDY;

/* window settings: position & initial size window */
GLint xxx = 25;
GLint yyy = 25;


void Graphics();
void ShowIt(void);
void DrawStrokeString(void *font, const char *string);
int MeasureStrokeLength(void *font, const char *string);

void InitGraph(void); //opens window and initializes screen
void MyReshape(int w, int h);

void MyKeys(unsigned char key, int x, int y);
void MySpecialKeys(int key, int x, int y);
void Quit();
void Further();
void Closer();
void Left();
void Right();
void Up();
void Down();

//Function definitions

void Graphics() //actual definition of model output called in ShowIt() and outputted each GlutStep()
{
  for(int a=0; a<NUMBER_FORAGERS; a++)
    {
      float x = foragers[a].px;
      float y = foragers[a].py;
      glColor3f(0.0,0.0,0.0);
      glRectd(x-0.5, y-0.5, x+0.5, y+0.5);
    }

 for(int a=0; a<NUMBER_PREDATORS; a++)
    {
      float x = predators[a].px;
      float y = predators[a].py;
      glColor3f(0.0,0.0,0.0);
      glRectd(x-1.0, y-1.0, x+1.0, y+1.0);

      //panel sepration lines
      glBegin(GL_LINES);
      glVertex2f(0,GRIDY/2);
      glVertex2f(predators[a].px,predators[a].py);
      glEnd();
    }

 float x,y;
 float radius = GROUP_RADIUS;

 //group spread - circle
 glBegin(GL_LINES);
 glColor3f(0.0f,0.0f,0.0f);
 x = (GRIDX/2) + ((float)radius * cos(359 * PI/180.0f));
 y = (GRIDX/2) + ((float)radius * sin(359 * PI/180.0f));
 for(int j = 0; j < 360; j++)
   {
     glVertex2f(x,y);
     x = (GRIDX/2) + ((float)radius * cos(j * PI/180.0f));
     y = (GRIDX/2) + ((float)radius * sin(j * PI/180.0f));
     glVertex2f(x,y);
   }
 glEnd();

 //maxdetection range - circle
 radius = 50+GROUP_RADIUS;
 glBegin(GL_LINES);
 glColor3f(0.0f,0.0f,0.0f);
 x = (GRIDX/2) + ((float)radius * cos(359 * PI/180.0f));
 y = (GRIDX/2) + ((float)radius * sin(359 * PI/180.0f));
 for(int j = 0; j < 360; j++)
   {
     glVertex2f(x,y);
     x = (GRIDX/2) + ((float)radius * cos(j * PI/180.0f));
     y = (GRIDX/2) + ((float)radius * sin(j * PI/180.0f));
     glVertex2f(x,y);
   }
 glEnd();

}

//GLUT FUNCTIONS

void GlutStep()
{
  glutPostRedisplay();
  glutMainLoopEvent(); //runs one iteration of graphics output based on glutDisplayFunc(ShowIt);
}


void initGLUT(int argc, char *argv[]) {
  glutInit(&argc,argv);                        //GLUT: initialize openGL via glut
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB); //GLUT
  glutInitWindowPosition(xxx, yyy);            //GLUT: define window position
  glutInitWindowSize(800,800);                 //GLUT: define window size : this should be made more flexible to fit screens
  glutCreateWindow(argv[0]);                   //GLUT: create window
  InitGraph();                                 //calls more GLUT functions
  glutIdleFunc(NULL);
  glutMotionFunc(NULL);
  glutPassiveMotionFunc(NULL);
  glutVisibilityFunc(NULL);
  glutEntryFunc(NULL);
  glutSpaceballMotionFunc(NULL);
  glutSpaceballRotateFunc(NULL);
  glutSpaceballButtonFunc(NULL);
  glutButtonBoxFunc(NULL);
  glutDialsFunc(NULL);
  glutTabletMotionFunc(NULL);
  glutTabletButtonFunc(NULL);
  glutMenuStatusFunc(NULL);

  glutReshapeFunc(MyReshape);                //GLUT: define special functions
  glutKeyboardFunc(MyKeys);                  //defines input variables for interaction with graphics output
  glutSpecialFunc(MySpecialKeys);
  glutDisplayFunc(ShowIt);                   //displays graphics output function
}

void InitGraph(void)
{
  glClearColor (1.0, 1.0, 1.0, 1.0);         //define clearing color
  glShadeModel (GL_FLAT);                    //define shape
  glMatrixMode(GL_MODELVIEW);                //define matrixmode
  glLoadIdentity();                          //define loadidentity
  glTranslatef(-0.5*(GLfloat)GRIDX,-0.5*(GLfloat)GRIDY,0.0); //how coordinates are translated to figure
  rheight=gridy;                             //define height and width
  rwidth=gridx;
}

void MyReshape(int w, int h)
{
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (w*GRIDY <= h*GRIDX)
    glOrtho (-0.5*GRIDX, 0.5*GRIDX, -0.5*GRIDX*(GLfloat)h/(GLfloat)w,
	     0.5*GRIDX*(GLfloat)h/(GLfloat)w, -1.0, 1.0);
  else
    glOrtho (-0.5*GRIDY*(GLfloat)w/(GLfloat)h, 0.5*GRIDY*(GLfloat)w/(GLfloat)h, -0.5*GRIDY, 0.5*GRIDY, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
  rwidth=w;
  rheight=h;
  factor=increase;
  }

// For updating graphics

void ShowIt(void)
{
  int strokelength;

  glClear(GL_COLOR_BUFFER_BIT);
  glColor3f(0.0,0.0,0.0);

  Graphics();

 /* this plots text in the window */
 glColor3f(0.0,0.0,0.0);
 strokelength=MeasureStrokeLength(GLUT_STROKE_ROMAN,INFORMATION);
 glPushMatrix();
 glTranslatef((GLfloat)GRIDX-0.1*(GLfloat)strokelength-15.,(GLfloat)GRIDY-10., 0.0);
 glScalef(0.05,0.05,0.05);
 DrawStrokeString(GLUT_STROKE_ROMAN,INFORMATION);
 glPopMatrix();

 if (!background)
   glutSwapBuffers();
}

void DrawStrokeString(void *font, const char *string) //used in ShowIt
{
  int i;

  for (i = 0; string[i]; i++)
    {
      glutStrokeCharacter(font, string[i]);
      if (string[i]==' ')
	glTranslatef(-MeasureStrokeLength(font," "),0.,0.);
    }
}

int MeasureStrokeLength(void *font, const char *string) //used in ShowIt
{
  int i;
  int stringlength=0.;

  for (i = 0; string[i]; i++)
    {
      if (string[i] == ' ')
	stringlength += 0.5 * glutStrokeWidth(font, string[i]);
      else
	stringlength += glutStrokeWidth(font, string[i]);
    }
  return(stringlength);
}


/****************************************************************/
/*                       Key Functions                          */
/****************************************************************/

void MyKeys(unsigned char key, int x, int y)
{
  switch (key)
    {
    case '\33' : Quit(); break;  //escape key
    case '-' : Further(); break;
    case '+' : Closer(); break;
    }
}

void MySpecialKeys(int key, int x, int y)
{
  switch (key)
    {
    case GLUT_KEY_LEFT : Left(); break;
    case GLUT_KEY_RIGHT : Right(); break;
    case GLUT_KEY_UP : Up(); break;
    case GLUT_KEY_DOWN : Down(); break;
    }
}

void Closer()
{
  factor *= (100. / (100. + increase));
  glMatrixMode(GL_PROJECTION);
  glScalef((1 + increase / 100.), (1 + increase / 100.), 1.0);
  glMatrixMode(GL_MODELVIEW);
  glutPostRedisplay();
}

void Further()
{
  factor *= (1 + increase / 100.);
  glMatrixMode(GL_PROJECTION);
  glScalef((100. / (100. + increase)), (100. / (100.+increase)), 1.0);
  glMatrixMode(GL_MODELVIEW);
  glutPostRedisplay();
}

void Up()
{
  glTranslatef(0.0, -factor / 100. * GRIDY, 0.0);
  glutPostRedisplay();
}

void Down()
{
  glTranslatef(0.0, factor / 100. * GRIDY, 0.0);
  glutPostRedisplay();
}

void Left()
{
  glTranslatef(factor / 100. * GRIDX, 0.0, 0.0);
  glutPostRedisplay();
}

void Right()
{
  glTranslatef(-factor / 100. * GRIDX, 0.0, 0.0);
  glutPostRedisplay();
}

void Quit()
{
  if (one_step) system("xset r");
  cout<<"********************************\n";
  cout<<"*   PROGRAM TERMINATED [ESC}   *\n";
  cout<<"********************************\n";
  exit(0);
}


