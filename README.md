Program name: Vigilance_advantage

Copyright 2013 Daniel J. van der Post

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

MODEL DESCRIPTION

Please see: van der Post et al. (2013) A novel mechanism for a survival advantage of vigilant individuals in groups. American Naturalist 182(5):682-688. <http://www.jstor.org/stable/10.1086/673298>

RUNNING THE MODEL

This model is run (e.g. in linux terminal) with graphics display with the following command (numbers are only examples and can be changed):

./model -S 5 -V 0.1 -N 2 -G 5 -R 10 -A 1000 -C 0.1 -P 0 -Y 1.0 -H 10 -Z 3 -F 1

It is run without graphics with:

./model -S 5 -V 0.1 -N 2 -G 5 -R 10 -A 1000 -C 0.1 -P 0 -Y 1.0 -H 10 -Z 3 -F 1 -b

Model options:

-S : random seed; any integer

-V : vigilance; value between 0 and 1, determines probability vigilant individual scans for predator

-G : group size; integer values between 1 and MAX_FORAGERS=200

-N : number of vigilant individuals; integer values between 1 and G

-R : radius of group; any values from 0, maximum not specified

-A : number of attacks; integer values from 0, maximum not specified

-C : change of positions; probability [0-1] that an individual is assigned new random position within the group radius

-P [1 or !1] = if 1 then does pairwise simulation of a vigilant and non-vigilant forager, with condition G=2, N=1

-Y [0-1] = alpha; saturation point of detection function (max detection rate)

-H [positive value] = h; halfmax value of detection function

-Z [positive value] = N; power that scales steepness of detection function

-F [1 or !1] = if 1 then does sigmoid detection function, else linear

-b : background toggle; when included, no graphics output is generated and program can run in the background

COMPILING THE MODEL

Requires various C++ packages and freeglut.

In LINUX the model can be compiled using the makefile via:

make

In WINDOWS we have been able compile and run the model using CodeBlocks
