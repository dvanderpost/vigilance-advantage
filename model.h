/*
Copyright 2013 Daniel J. van der Post

This file is part of Vigilance_advantage.

Vigilance_advantage is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Vigilance_advantage is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MODEL_H_GUARD_
#define _MODEL_H_GUARD_

#include "par.h"
#include <iostream>

using std::cout;
using std::endl;
using std::cin;

void Start(int argc, char *argv[]); //used in main.cpp
void Initialize();                  //used in main.cpp
void Run();                         //used in main.cpp

enum action_type {MOVE, FLEE, NOTHING, SCAN, ATTACK};

class Animal_type {

 public:
  int id;
  action_type action;
  float px, py;
  float hx,hy;
  int comptime;
  float pV;
  int predation_events;
  int scan_events;
  float avg_dist_vig_caught;
  float avg_dist_vig_target;
  float avg_nearest_dist_vig;
  int target_events;

  //functions	
  void Init(float ppx, float ppy, float hhx, float hhy, float ppV, int* idd, int simtime);
  float Distance(float exx, float eyy);
};

#endif // _MODEL_H_GUARD_
