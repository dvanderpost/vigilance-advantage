/*
Copyright 2013 Daniel J. van der Post

This file is part of Vigilance_advantage.

Vigilance_advantage is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Vigilance_advantage is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _PAR_H_GUARD_
#define _PAR_H_GUARD_

#include <math.h>

//system settings
const int QUITTIME  = 10000000; /* time when program quits */
const int GRIDX = 300;           /* gridsize on x-axis */
const int GRIDY = 300;           /* gridsize on y-axis */
const int MAX_FORAGERS = 200;
const int NUMBER_PREDATORS = 1;

//predator behavior
const float PRED_ATTACK_DISTANCE = 5.0;
enum target_type {CLOSEST, FURTHEST, RANDOM};
const target_type predatortargetting = CLOSEST; //in undefined, default is CLOSEST; FURTHEST + RANDOM only for fixed groups

//placement of foragers
//const bool PAIRWISE = false;           //if true then places only two individuals + at GROUPRADIUS from each other
const bool RANDOMVECTOR = false;           //if true place individuals using random vector, otherwise uniformly

//predator detection function
const float MAX_DETECT_DISTANCE = 1000.0;
//const bool SIGMOID = true;                //if true: sigmoid predator detection function else linear (alpha*(H-distance))  
//const float ALPHA = 1.0;
//const float NN = 2.0;
//const float H = 20.0;
//const float H_N = pow(H,NN);

//constants
const float PI = 3.141592654;

#endif // _PAR_H_GUARD_
